package com.zhao.apitest.source;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @author ZhaoPan
 * @date 2022/3/9
 * @describe 读取文件数据
 */
public class SourceTest2_File {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env=StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //从文件读取数据
        DataStream<String> dataStream = env.readTextFile("/Volumes/Update/flink/flink_test/src/main/resources/sensor.txt");

         dataStream.print();

         env.execute();
    }
}
