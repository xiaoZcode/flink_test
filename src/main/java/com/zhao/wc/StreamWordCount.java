package com.zhao.wc;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

//流处理 （DataStream）支持实时数据
public class StreamWordCount {
    /**
     * @author ZhaoPan
     * @createTime 2022/3/2
     * @description
     */
    public static void main(String[] args) throws Exception {
        //创建流处理环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //设置并行度 相当于8个线程
        //env.setParallelism(2);

        //从文件中读取数据
        //String inputPath = "E:\\flinkTest\\src\\main\\resources\\text.txt";
        //DataStream<String> inputDataSream = env.readTextFile(inputPath);

        //从程序启动参数中提起数据
        //ParameterTool parameterTool = ParameterTool.fromArgs(args);
        //String host = parameterTool.get("host");
        //int port = parameterTool.getInt("port");
        ////从socket文本流读取数据
        //DataStreamSource<String> inputDataSream = env.socketTextStream(host, port);

        DataStreamSource<String> inputStream = env.socketTextStream("localhost", 7777);
        //基于数据流进行转换计算
        DataStream<Tuple2<String, Integer>> resultStream = inputStream.flatMap(new WordCount.MyflatMapper())
                .keyBy(0)
                .sum(1);

        resultStream.print();

        //执行任务
        env.execute();
    }
}
